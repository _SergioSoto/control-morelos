# Control Morelos #


### Actor: Luis

Luis quiere ayudar y mandar viveres a zonas afectadas en su estado (Morelos), sin embargo el no sabe a ciencia cierta donde hay centros de acopio en su municipio.

Actualmente Luis tiene que buscar en una hoja de excel la informacion de cada centro de acopio, la hoja de excel puede estar desactualizada y no es facil visualizar la localizacion de los centros ni las necesidades que existen en cada uno. Como Luis es un millenial, quiere hacer todo lo mas rapido posible y no tiene ganas de andar "cazando" lugares para ayudar. Lo enredado del proceso puede desmotivarlo y hacer que se olvide del tema.


### Problemas

* Algunos centros de acopio son muy "populares" mientras otros son ignorados por desconocimiento de su existencia.
* Existen diferentes necesidades en cada centro de acopio. eg. a unos les faltan botellas de agua, a otros cobijas, unos estan llenos, etc.

Como puede Luis asegurarse de que su ayuda llegue a centros que lo necesitan mas?


### Caso de uso:

Luis entra a un sitio web donde ve un mapa interactivo de Morelos, le da click a su municipio y puede ver una lista con los centros de acopio en ese municipio con informacion acerca de las necesidades de cada centro. Eso lo va ayudar a decidir de una forma rapida en que centro se puede aprovechar mejor su donativo.

La informacion viene de una hoja de calculo en google docs, que cada centro puede editar deacuerdo a sus necesidades. Los cambios en la lista son reflejados al momento en el sitio web. Cada centro esta pre-autorizado con el objetivo de evitar fraudes.


### Must Have

* Al darle click al mapa, se muestra una lista con centros de acopio por municipio
* Arquitectura de software flexible que permita expander sistema a otros estados

### Nice to Have

* Al entrar al sitio web, los municipios mas necesitados estan de color rojo
* Los mapas utilizan la API de google maps para trazar rutas
* Estados de Oaxaca y Chiapas

### Retos

- Relacion cercana con centros de acopio que permita la adopcion de la plataforma
- Supervision de hoja de calculo para evitar vandalismo o fraudes

### Retos Tecnicos

- Interpretar automaticamente hoja de calculo
- Diseno grafico de la aplicacion

