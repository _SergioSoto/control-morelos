import os
import gspread

from oauth2client.service_account import ServiceAccountCredentials
from flask import Flask, render_template, request

from apoyo_morelos import ApoyoMorelos


current_folder = os.path.dirname(os.path.abspath(__file__))
secret = os.path.join(current_folder, 'client_secret.json')

scope = ['https://spreadsheets.google.com/feeds']
creds = ServiceAccountCredentials.from_json_keyfile_name(secret, scope)
client = gspread.authorize(creds)

sheet = client.open('Copy of ApoyoMorelos').sheet1
apoyo_morelos =  ApoyoMorelos(sheet.get_all_values())

app = Flask(__name__)

@app.route('/')
def result():
    return render_template(
        'index.html',
        announcements = apoyo_morelos.announcement,
        entries = apoyo_morelos.entries
    )

@app.route('/entries/<municipio>')
def load_entries(municipio):
    selected_entries = []
    if municipio == 'all':
        selected_entries = apoyo_morelos.entries
    else:
        for entry in apoyo_morelos.entries:
            if municipio in entry['lugar'].lower() or entry['lugar'].lower() in municipio:
                selected_entries.append(entry)

    return render_template(
        'municipio.html',
        name = selected_entries[0]['lugar'],
        announcements = apoyo_morelos.announcement,
        entries = selected_entries
    )

def get_priority_color(municipio):
    priority = apoyo_morelos.get_priority(municipio)
    if priority == 'high':
        return '#f44242'
    elif priority == 'medium':
        return '#ffbc14'
    elif priority == 'low':
        return '#50c431'
    elif priority == 'no_data':
        return '#fefee9'
    elif priority == 'not_found':
        return '#a4a5a4'
    else:
        return '#ffffff'

app.jinja_env.globals.update(get_priority_color=get_priority_color)


if __name__ == '__main__':
   app.run(debug = True)