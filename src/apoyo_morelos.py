import collections

class ApoyoMorelos:

    def __init__(self, sheet_content):
     	self.sheet_content = sheet_content
        self.announcement = self.get_announcement()
        self.entries = self.get_entries()

    def get_announcement(self):
        return self.sheet_content[1][1]

    def get_entries(self):
    	start_row = 5
    	entries = []
    	for row in self.sheet_content[start_row:]:
    		entry = collections.OrderedDict()
    		entry['fecha'] = row[1]
    		entry['urgencia'] = row[3]
    		entry['lugar'] = row[2]
    		entry['direccion'] = row[10]
    		entry['centro'] = row[6]
    		entry['requerimientos'] = row[5]
    		entry['no_llevar'] = row[7]
    		entry['referentes'] = row[4]
    		entry['contacto'] = row[9]
    		entry['comentarios'] = row[8]
    		entries.append(entry)
    	return entries

    def get_priority(self, name):
        priority = None
        matching_entries = [entry for entry in self.entries \
                            if entry['lugar'].lower() in name.lower() \
                            or name.lower() in entry['lugar'].lower()]

        if len(matching_entries) == 0:
            return 'not_found'

        for entry in matching_entries:
            if 'URGENCIA ALTA' in entry['urgencia'].upper():
                priority = 'high'
            elif 'URGENCIA MEDIA' in entry['urgencia'].upper():
                if priority is not 'high':
                    priority = 'medium'
            elif 'URGENCIA BAJA' in entry['urgencia'].upper():
                  if priority is not 'high' or priority is not 'medium':
                    priority = 'low'

        if priority is None:
            return 'no_data'
        else:
            return priority
